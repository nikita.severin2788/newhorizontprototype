﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    public Transform CharacterTransform;

    [SerializeField] private float OffsetX;
    [SerializeField] private float OffsetY;
    [SerializeField] private float OffsetZ;
    [SerializeField] private float Speed;


    void Start()
    {
        CharacterTransform = CharacterController.Instance.GetComponent<Transform>();
    }

    void FixedUpdate()
    {
        Vector3 CharacterPostion = CharacterTransform.position;
        transform.position = Vector3.Lerp(transform.position,new Vector3(CharacterPostion.x + OffsetX,CharacterPostion.y + OffsetY,CharacterPostion.z + OffsetZ),Speed * Time.deltaTime);

    }
}
