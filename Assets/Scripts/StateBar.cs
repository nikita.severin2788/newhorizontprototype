﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateBar : MonoBehaviour
{

    public int Oxygen;
    public int Healtch;

    public Image HealtchBar;
    public Image OxygenBar;

    public float SpeedLerp;

    private void OnEnable() {
        Character.OxygenUpdate += OxygenUpdate;
        Character.HealtchUpdate += HealtchUpdate;
    }

    private void OnDisable() {
        Character.OxygenUpdate -= OxygenUpdate;
        Character.HealtchUpdate -= HealtchUpdate;
    }

    public void OxygenUpdate(int count) {
        Oxygen = count;
    }

    public void HealtchUpdate(int count) {
        Healtch = count;
    }

    private void Update() {
        HealtchBar.fillAmount = Mathf.Lerp(HealtchBar.fillAmount,Healtch / 100f,SpeedLerp * Time.deltaTime);
        OxygenBar.fillAmount = Mathf.Lerp(OxygenBar.fillAmount,Oxygen / 100f,SpeedLerp * Time.deltaTime);
    }
}
