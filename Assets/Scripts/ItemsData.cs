﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsData : MonoBehaviour
{

    public static ItemsData Instance;

    public List<Item> MainItmes = new List<Item>();
    public List<Item> ItemInventory = new List<Item>();

    private void Awake() {
        Instance = this;
    }

    public Item GetMainItem(string name) {

        foreach(Item SetItem in MainItmes) {
            if(SetItem.Name == name) {
                return SetItem;
            }
        }
        Debug.Log("Item: " + name + " not found!");
        return null;
    }

    public Item GetStorageItem(string name) {

        foreach(Item SetItem in ItemInventory) {
            if(SetItem.Name == name) {
                return SetItem;
            }
        }
        Debug.Log("Item: " + name + " not found!");
        return null;
    }

    public bool CheckItemByName(string name) {

        foreach(Item SetItem in ItemInventory) {
            if(SetItem.Name == name)
                return true;
            else
                return false;
        }
        Debug.Log("Item: " + name + " not found!");
        return false;
    }

    public bool CheckItemById(int id) {
        if(ItemInventory.Count > id) 
             return true;
        else 
            return false;
    }

    public int CheckItemCountById(int id) {
        return ItemInventory[id].Count;
    }

    public bool CheckFreeInventoryItem() {
        if(ItemInventory.Count <= 8) {
            return true;
        } else {
            return false;
        }
    }

    public bool CheckFreeCountInItem (string name) {

        foreach(Item SetItem in ItemInventory) {
            if(SetItem.Name == name) {
                if(SetItem.Count < 5)
                    return true;
            }
        }
        return false;
    }
   

    public void AddStorageItem(string name) {
        if(ItemInventory.Count <= 8) {
            for(int i = 0; i < MainItmes.Count; i++) {
                if(MainItmes[i].Name == name) {
                    ItemInventory.Add(new Item (MainItmes[i].Name,MainItmes[i].Image,MainItmes[i].Prefab,MainItmes[i].Count,MainItmes[i].Description));
                }
            }
        }
    }

    public void AddItemCount(string name) {
        for(int i = 0; i < ItemInventory.Count; i++) {
            if(ItemInventory[i].Name == name) {
                if(ItemInventory[i].Count < 5) {
                    ItemInventory[i].Count++;
                    break;
                }
            }
        }
    }

    public void AddItemCount(string name, int count) {
        for(int i = 0; i <= ItemInventory.Count; i++) {
            if(ItemInventory[i].Name == name) {
                if(ItemInventory[i].Count < 5)
                ItemInventory[i].Count += count;
            }
        }
    }
    
    public void RemoveItemByName(string Name) {
        for(int i = 0; i <= ItemInventory.Count; i++) {
            if(ItemInventory[i].Name == name) {
                ItemInventory.RemoveAt(i);
            }
        }
    }
    public void RemoveItemById(int id) {
        ItemInventory.RemoveAt(id);
    }
    public void RemoveItemCountById(int Id) {
        ItemInventory[Id].Count--;
    }

    public void RemoveItemCount(string name) {
        for(int i = 0; i < ItemInventory.Count; i++) {
            if(ItemInventory[i].Name == name) {
                ItemInventory[i].Count--;
                if(ItemInventory[i].Count < 1) {
                    RemoveItemById(i);
                }
                break;
            }
        }
    }

    public void RemoveItemCount(string name, int count) {
        for(int i = 0; i < ItemInventory.Count; i++) {
            if(ItemInventory[i].Name == name) {
                ItemInventory[i].Count -= count;
            }
        }
    }

    public void RemoveEmptyItems() {
        for(int i = 0; i < ItemInventory.Count; i++) {
            if(ItemInventory[i].Count < 1) {
                ItemInventory.RemoveAt(i);
                RemoveEmptyItems();
            }
        }
    }

    public int CountByName(string name) {
        foreach(Item SetItem in ItemInventory) {
            if(name == SetItem.Name)
                return SetItem.Count;
        }
        return 0;
    }

    public GameObject GetObjectByID(int Id) {
        return ItemInventory[Id].Prefab;
    }

    public int ReturnCounInElementByID(int id) {
        return ItemInventory[id].Count;
    }
    public int RetrunCountElelments() {
        return ItemInventory.Count;
    }

    public Sprite GetItemSprite(int id) {
        return ItemInventory[id].Image;
    }

    public bool CheckFree(string name) {
        if(CheckItemByName(name)) {
            if(CheckFreeCountInItem(name)) {
                return true;
            } else {
                if(CheckFreeInventoryItem())
                    return true;
                else
                    return false;
            }
        } else {
            if(CheckFreeInventoryItem())
                return true;
            else
                return false;
        }
    }

    public void AddElement(string name) {
       if(CheckFreeInventoryItem()) {
            if(CheckItemByName(name)) {
                if(CheckFreeCountInItem(name)) {
                    AddItemCount(name);
                } else {
                    AddStorageItem(name);
                }
            } else {
                AddStorageItem(name);
            }
        } else {
            if(CheckItemByName(name)) {
                if(CheckFreeCountInItem(name)) {
                    AddItemCount(name);
                }
            }
        }
    }

    public void RemoveElemetn(int id) {
        if(CheckItemById(id)) {
            RemoveItemCountById(id);
            if(CheckItemCountById(id)<= 0) {
                RemoveItemById(id);
            }
        }
    }

}


[System.Serializable]
public class Item{

    public string Name;
    public Sprite Image;
    public GameObject Prefab;
    public int Count;
    public string Description;

    public Item(string _name,Sprite _image, GameObject _prefab, int _count, string _description) {
        Name = _name;
        Image = _image;
        Prefab = _prefab;
        Count = _count;
        Description = _description;
    }
}
