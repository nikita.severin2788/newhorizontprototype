﻿using UnityEngine;
using UnityEngine.Events;

public class State : MonoBehaviour
{
    [SerializeField]
    private StateType _state;

    public static State Instance;

    public static event UnityAction<StateType> Event;


    private void Awake() {
        Instance = this;
    }

    private void Start() {
        _state = StateType.none;
        Event?.Invoke(_state);
    }

    public void SetState(StateType state) {
        _state = state;
        Event?.Invoke(_state);
    }

    public StateType GetState() {
        return _state;
    }

    public bool isEqual(StateType state) {
        if(state == _state)
            return true;
        else
            return false;
    }
}

public enum StateType {
    none,
    jump,
    inventroy,
    dead
}
