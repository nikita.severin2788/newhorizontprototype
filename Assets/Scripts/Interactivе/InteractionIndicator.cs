﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionIndicator : MonoBehaviour
{

    private Camera CameraMain;
    private Canvas CanvasMain;

    [SerializeField]
    public GameObject Indicator;

    private GameObject SetIndicator;

    private void Start() {
        CameraMain = Camera.main;
        CanvasMain = FindObjectOfType<Canvas>();
    }

    public void Enable() {
        Spawn();
        StartCoroutine(UpdateElement());
    }

    public void Disable() {
        StopCoroutine(UpdateElement());
        Destroy();
    }

    private void Spawn() {
        SetIndicator = Instantiate(Indicator,CanvasMain.transform);
        
    }

    private void Destroy() {
        Destroy(SetIndicator);
    }


    private IEnumerator UpdateElement() {

        while(true) {
            yield return new WaitForFixedUpdate();
            SetIndicator.transform.position = CameraMain.WorldToScreenPoint(transform.position);
        }
        
    }
}
