﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ObjectItem:MonoBehaviour {

    public string PropName = "None";

    public TakeAnimation takeAnimation;
    public InteractionIndicator interactionIndicator;
    public GameObject Trigger;


    public void Take() {
        Destroy(Trigger);
        interactionIndicator.Disable();
        takeAnimation.Action();
    }
}
