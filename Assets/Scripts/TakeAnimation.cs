﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeAnimation : MonoBehaviour
{


    public float SpeedAnimation = 10f;
    public float ScalePercent_1 = 0.4f;
    public float ScalePercent_2 = 0.4f;
    public float ScalePercent_3 = 0.1f;

    public bool AddRotation;
    public Vector3 AddRotationAngle;

    private float SetScalePercent;

    private Quaternion SetRotaion;

    private Vector3 StartScale;
    private Vector3 SetPostion;
    private Vector3 CharacterPositon;



    public void Action() {

         StartCoroutine("TakeAction");
         StartCoroutine("CoroutineUpdate");
    }



    IEnumerator CoroutineUpdate() {
        while(true) {
            CharacterPositon = CharacterController.Instance.CurrentPosition;

            SetPostion = new Vector3(CharacterPositon.x,SetPostion.y,CharacterPositon.z);

            transform.localScale = Vector3.Lerp(transform.localScale,StartScale * SetScalePercent,SpeedAnimation * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position,SetPostion,SpeedAnimation * Time.deltaTime);

            if(AddRotation)
                transform.rotation = Quaternion.Lerp(transform.rotation,SetRotaion,SpeedAnimation * Time.deltaTime);

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator TakeAction() {

        SetRotaion = transform.rotation;
        StartScale = transform.localScale;

        SetScalePercent = ScalePercent_1;
        SetPostion = new Vector3(transform.position.x,transform.position.y + 2f,transform.position.z);
        yield return new WaitForSeconds(0.4f);
        SetRotaion = Quaternion.Euler(AddRotationAngle.x,AddRotationAngle.y,AddRotationAngle.z);
        SetPostion = new Vector3(CharacterPositon.x,CharacterPositon.y + 3.5f,CharacterPositon.z);
        SetScalePercent = ScalePercent_2;
        yield return new WaitForSeconds(0.5f);
        SpeedAnimation = 15f;
        SetPostion = new Vector3(CharacterPositon.x,CharacterPositon.y + 1f,CharacterPositon.z);
        SetScalePercent = ScalePercent_3;
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
        yield return true;
    }
}
