﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{

    public GameObject UIPrefab;
    public GameObject InstanceIUPrefab;

    [SerializeField]
    private List<Cell> CellList = new List<Cell>();
    [SerializeField]
    private List<Vector2> EnablePostiion = new List<Vector2>();
    [SerializeField]
    private List<Vector2> EnablePositionTransition = new List<Vector2>();

    

    private string _State;


    public float SpeedAnimation;
    public float SpeedColorAnimation;
    public bool isUIDisable;

    public float StandartColor;
    public float TransparentColor;

    public Vector2 SlectScale;
    public Vector2 DefaultScale;

    public Sprite SelectCellSprite;
    public Sprite UnselectedCellSprite;
    public Sprite NullItemSprite;

    public int CellRotateNumber = 1;
    public int SelectCellNumber=0;

    public GameObject PrefabInventory;
    public GameObject InstancePrefabInventory;


    public bool isShow;
    /*
    public void OnEnable() {
        State.Event += StateUpdate;
    }

    public void OnDisable() {
        State.Event -= StateUpdate;
    }
    */
    void Update() {
        if(Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.JoystickButton5)) {
            if(_State == "None") {
                if(!isShow)
                    StartCoroutine(EnableUI());
                else
                    StartCoroutine(DisableUI());
            }
        }
            if(isShow) {
            if(Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.JoystickButton6)) {
                BackCell();
                SoundEffect();
            }

            if(Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.JoystickButton7)) {
                NextCell();
                SoundEffect();
            }

            if(Input.GetKeyDown(KeyCode.F) || Input.GetKeyDown(KeyCode.JoystickButton7))
                DropProp();
        }

        if(InstancePrefabInventory != null)
            InstancePrefabInventory.transform.position = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x,transform.position.y + 1f,transform.position.z));

        if(InstancePrefabInventory != null) {
            for(int i = 0; CellList.Count > i; i++) {
                CellList[i].CellTransform.localPosition = Vector2.Lerp(CellList[i].CellTransform.localPosition,EnablePositionTransition[i],SpeedAnimation * Time.deltaTime);
            }
            for(int i = 0; CellList.Count > i; i++) {
                CellList[i].CellTransform.localScale = Vector2.Lerp(CellList[i].CellTransform.localScale,new Vector2(1,1) * CellList[i].Scale,SpeedAnimation * Time.deltaTime);
            }

            for(int i = 0; CellList.Count > i; i++) {
                if(ItemsData.Instance.RetrunCountElelments() > i) {
                    CellList[i].CountText.text = ItemsData.Instance.ReturnCounInElementByID(i).ToString();
                    CellList[i].ItemImage.sprite = ItemsData.Instance.GetItemSprite(i);
                } else {
                    CellList[i].CountText.text = " ";
                    CellList[i].ItemImage.sprite = NullItemSprite;
                }
            }

        }
            
    }

    private void SoundEffect() {
        InstancePrefabInventory.GetComponent<AudioSource>().Play(0);
       }

    public void SpawnInventory() {
        InstancePrefabInventory = Instantiate(PrefabInventory,FindObjectOfType<Canvas>().transform);
    }
    public void DestroyInventory() {
        Destroy(InstancePrefabInventory);
    }

    IEnumerator EnableUI() {
        SpawnInventory();

        //for(int i = 0; EnablePositionTransition.Count > i; i++) {
        //    EnablePositionTransition[i] = EnablePostiion[i];
        //}
        for(int i = 0; i < CellList.Count; i++) {
            CellList[i].CellTransform = InstancePrefabInventory.transform.GetChild(i).GetComponent<RectTransform>();
           
        }
        for(int i = 0; i < CellList.Count; i++) {
            CellList[i].ItemImage = CellList[i].CellTransform.GetChild(0).GetComponent<Image>();
        }
        for(int i = 0; CellList.Count > i; i++) {
            Transform ChildObject = CellList[i].CellTransform.GetChild(1).transform;
            CellList[i].CountText = ChildObject.GetChild(0).GetComponent<Text>();
        }
        if(InstancePrefabInventory != null) {
            for(int i = 0; CellList.Count > i; i++) {
                CellList[i].CellTransform.localPosition = EnablePositionTransition[i];
            }
        }
        //CellList[CellRotateNumber].Scale = 1.5f;

        //CellList[CellRotateNumber].CellTransform.localScale = new Vector2(1,1) * CellList[CellRotateNumber].Scale;
        

        isShow = true;
        
        yield return true;
    }

    IEnumerator UpdateUI() {
        while(true) {
            if(InstancePrefabInventory != null)
                InstancePrefabInventory.transform.position = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x, transform.position.y +1f, transform.position.z));

            if(InstancePrefabInventory != null) {
                for(int i = 0; CellList.Count > i; i++) {
                    CellList[i].CellTransform.localPosition = Vector2.Lerp(CellList[i].CellTransform.localPosition,EnablePositionTransition[i],SpeedAnimation * Time.deltaTime);
                }
                for(int i = 0; 8 > i; i++) {
                    CellList[i].CellTransform.localScale = Vector2.Lerp(CellList[i].CellTransform.localScale,new Vector2(1,1) * CellList[i].Scale, SpeedAnimation * Time.deltaTime);
                }
                for(int i = 0; CellList.Count > i; i++) {
                    if(ItemsData.Instance.RetrunCountElelments() <= i) {
                        CellList[i].CountText.text = ItemsData.Instance.ReturnCounInElementByID(i).ToString();
                    }
                }
            }
            

           yield return new WaitForSeconds(0.02f);
        }
    }


    IEnumerator DisableUI() {

        for(int i = 0; CellList.Count < i; i++) {
            EnablePositionTransition[i] = Vector2.zero;
        }

        isShow = false;
        DestroyInventory();
        yield return true;
    }




    public void NextCell() {

        if(CellRotateNumber+1 > 7)
            CellRotateNumber = 0;
        else
            CellRotateNumber++;

        Debug.Log(CellRotateNumber);

        int Index = CellRotateNumber;
        for(int i = 0; EnablePositionTransition.Count > i; i++) {
            EnablePositionTransition[i] = EnablePostiion[Index];
            if((Index + 1) > 7) {
                Index = 0;
            } else {
                Index += 1;
            }
        }

        if(SelectCellNumber - 1 < 0)
            SelectCellNumber = 7;
        else
            SelectCellNumber--;


        for(int i = 0; CellList.Count > i; i++) {
            if(i == SelectCellNumber) {
                CellList[i].Scale = 1.5f;
            } else {
                CellList[i].Scale = 1f;
            }
        }


    }

    public void BackCell() {

        if(CellRotateNumber-1 < 0)
            CellRotateNumber = 7;
        else
            CellRotateNumber--;

        Debug.Log(CellRotateNumber);

        int Index = CellRotateNumber;
        for(int i = 0; EnablePositionTransition.Count > i; i++) {
            EnablePositionTransition[i] = EnablePostiion[Index];
            if((Index + 1) > 7) {
                Index = 0;
            } else {
                Index += 1;
            }
            
        }

        if(SelectCellNumber + 1 > 7)
            SelectCellNumber = 0;
        else
            SelectCellNumber++;

        for(int i = 0; CellList.Count > i; i++) {
            if(i == SelectCellNumber) {
                CellList[i].Scale = 1.5f;
            } else {
                CellList[i].Scale = 1f;
            }
        }
    }
    public void DropProp() {
        if(ItemsData.Instance.CheckItemById(SelectCellNumber)) {
            PropsDropScript.Instance.DropObject(ItemsData.Instance.GetObjectByID(SelectCellNumber));
            ItemsData.Instance.RemoveElemetn(SelectCellNumber);
        }
    }
    /*
    public void StateUpdate(string state) {
        _State = state;
        if(_State != "None" || _State != "Inventory")
        StartCoroutine(DisableUI());
    }*/
}
[System.Serializable]
public class Cell {

    public GameObject Prefab;
    public RectTransform CellTransform;
    public Image ItemImage;
    public Vector2 EnabledCellPositon;
    public GameObject CounterPrefab;
    public Text CountText;
    public float Scale = 1f;

    public Cell(GameObject _prefab, RectTransform _cell,Image _itemImage,Vector2 _enabledCellsPosition, GameObject _counterPrefab, Text _countText ,float _scale) {
        Prefab = _prefab;
        CellTransform = _cell;
        ItemImage = _itemImage;
        EnabledCellPositon = _enabledCellsPosition;
        CounterPrefab = _counterPrefab;
        CountText = _countText;
        Scale = _scale;
    }
}