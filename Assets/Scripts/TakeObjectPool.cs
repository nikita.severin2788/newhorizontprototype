﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TakeObjectPool : MonoBehaviour
{


    public static TakeObjectPool Instance;

    public List<GameObject> Pool = new List<GameObject>();

    public static UnityAction ListUpdate;
    

    private void Awake() {
        Instance = this;
    }

    void Update()
    {
        if(Pool.Count > 1) {
            for(int i = 0; i < Pool.Count; i++) {
                for(int b = i+1; b < Pool.Count-1; b++) {
                    float ObjectDistanceI = Vector3.Distance(Pool[i].transform.position,transform.position);
                    float ObjectDistanceB = Vector3.Distance(Pool[b].transform.position,transform.position);
                    if(ObjectDistanceI < ObjectDistanceB) {
                        GameObject Buffer = Pool[b];
                        Pool[b] = Pool[i];
                        Pool[i] = Buffer;
                    }
                }
            }
        }
    }


    public bool FirstObject(GameObject Object) {
        if(Pool.Count > 0) {
            if(Object == Pool[0]) {
                return true;
            } else {
                return false;
            }
        } else {
            return false; 
        }
    }

    public void RemoveObject(GameObject Object) {
        StartCoroutine(RemoveDelay(Object));
    }

    IEnumerator RemoveDelay(GameObject Object) {
        yield return new WaitForSeconds(0.1f);
        for(int i = 0; i < Pool.Count; i++) {
            if(Pool[i] == Object) {
                Pool.RemoveAt(i);
            }
        }
        ListUpdate?.Invoke();
        yield return true;
    }

    public void AddObject(GameObject Object) {
        Pool.Add(Object);
    }
}
