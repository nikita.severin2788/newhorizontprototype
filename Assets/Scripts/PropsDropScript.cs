﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropsDropScript : MonoBehaviour
{
    public static PropsDropScript Instance;

    public Transform SpawnPoint;

    private void Awake() {
        Instance = this;
    }

    public void DropObject(GameObject Object) {
        GameObject _Object = Instantiate(Object,SpawnPoint.transform.position,Quaternion.Euler(0,0,0));
        _Object.GetComponent<Rigidbody>().AddForce(_Object.transform.forward * 1,ForceMode.Impulse);
    }
}
