﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSctipt:MonoBehaviour {

    public bool DoorLockedMode;

    public float CloseDelay = 1f;
    public float UpdateTime = 2f;

    public float Speed = 1f;


    public DoorPart[] Door = new DoorPart[1];


    private void Start() {
        for(int i = 0; i < Door.Length; i++) {
            Door[i].ClosePosition = Door[i].Door.transform.localPosition;

        }
    }

    public void Open() {
        StartCoroutine("OpenCorutine");
    }

    public void Closed() {
        StartCoroutine("CloseCorutine");
    }

    IEnumerator CloseCorutine() {
        StopCoroutine("OpenCorutine");
        yield return new WaitForSeconds(CloseDelay);
        for(int i = 0; i < Door.Length; i++) {
            Door[i].TargetPositon = Door[i].ClosePosition;
        }
        StartCoroutine("Update");
        yield return new WaitForSeconds(UpdateTime);
        StopCoroutine("Update");

    }
    IEnumerator OpenCorutine() {
        StopCoroutine("CloseCorutine");
        for(int i = 0; i < Door.Length; i++) {
            Door[i].TargetPositon = Door[i].OpenPosition;
        }
        StartCoroutine("Update");
        yield return new WaitForSeconds(UpdateTime);
        StopCoroutine("Update");

        yield return true;
    }

    IEnumerator UpdateElement() {
        while(true) {
            for(int i = 0; i < Door.Length; i++) {
                Door[i].Door.transform.localPosition = Vector3.MoveTowards(Door[i].Door.transform.localPosition,Door[i].TargetPositon,Speed * Time.deltaTime);
            }

            yield return new WaitForEndOfFrame();
        }
    }

    public void Unlocked() {
        DoorLockedMode = false;
    }
}

[System.Serializable]
public class DoorPart {

    public GameObject Door;
    public Vector3 ClosePosition;
    public Vector3 OpenPosition;
    public Vector3 TargetPositon;

    public DoorPart(GameObject _door,Vector3 _closePosition,Vector3 _openPosition,Vector3 _targetPostion) {
        Door = _door;
        ClosePosition = _closePosition;
        OpenPosition = _openPosition;
        TargetPositon = _targetPostion;
    }

}
