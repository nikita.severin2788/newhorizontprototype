﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GruardianManager : MonoBehaviour
{
    float f;
    public float FloatingSpeed;
    public Transform Yeye;
    public float x;
    public float y;
    public float z;
    public Transform RayPoint;

    RaycastHit hit;

    private void Update() {
        f = Mathf.Sin(Time.time)/2;

        Debug.DrawRay(RayPoint.position,RayPoint.transform.forward);

        transform.position = Vector3.Lerp(transform.position,new Vector3(transform.position.x,transform.position.y + f,transform.position.z),FloatingSpeed * Time.deltaTime);

        if(Physics.Raycast(RayPoint.position, RayPoint.transform.forward,out hit, 50f)) {
            Yeye.LookAt(new Vector3(hit.point.x,hit.point.y,hit.point.z -180f));
        }
    }
}
