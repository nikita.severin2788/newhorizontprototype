﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBoxScript : MonoBehaviour
{

    public List<GameObject> LootList = new List<GameObject>(3);

    public Quaternion OpenBox;
    public Quaternion ClosedBox;
    private Quaternion SelectedRotation;

    public float AnimationSpeed;
    public GameObject TopBox;
    public GameObject ActionUI;
    public GameObject Trigger;
    private bool OnAction;

    private GameObject InstanceIUPrefab;


    public void Action() {
        if(TakeObjectPool.Instance.FirstObject(gameObject)) {
            if(!OnAction) {
                if(Input.GetKeyDown(KeyCode.Joystick1Button4) || Input.GetKeyUp(KeyCode.R)) {
                        Disable();
                        ComponentsDisable();
                        StartCoroutine("TakeAction");
                    SoundEffect();
                        
                        Destroy(Trigger);
                        OnAction = true;
                }
            }
        }
    }

    private void SoundEffect() {
            GetComponent<AudioSource>().Play(0);
    }

    public void Enable() {
        TakeObjectPool.Instance.AddObject(gameObject);
    }

    public void UpdateUI() {
        if(TakeObjectPool.Instance.FirstObject(gameObject)) {
            if(InstanceIUPrefab == null)
                InstanceIUPrefab = Instantiate(ActionUI,FindObjectOfType<Canvas>().transform);
        } else {
            if(InstanceIUPrefab != null)
                Destroy(InstanceIUPrefab);
        }
        if(InstanceIUPrefab != null) {
            InstanceIUPrefab.transform.position = Camera.main.WorldToScreenPoint(transform.position);
        }
    }

    public void Disable() {
        if(InstanceIUPrefab != null) {
            Destroy(InstanceIUPrefab);
        }
        TakeObjectPool.Instance.RemoveObject(gameObject);
    }

    private void ComponentsDisable() {
        if(GetComponent<Collider>() != null)
            GetComponent<Collider>().enabled = false;

        if(GetComponent<Rigidbody>() != null)
            GetComponent<Rigidbody>().isKinematic = true;
    }

    IEnumerator CoroutineUpdate() {
        while(true) {

            TopBox.transform.rotation = Quaternion.Lerp(TopBox.transform.rotation,OpenBox, AnimationSpeed * Time.deltaTime);
            yield return new WaitForSeconds(0.005f);

        }
    }

    IEnumerator TakeAction() {
        TakeObjectPool.Instance.RemoveObject(gameObject);

        yield return new WaitForSeconds(0.35f);
        StartCoroutine("CoroutineUpdate");
        yield return new WaitForSeconds(0.4f);
        for(int i = 0; i < LootList.Count; i++) {
            GameObject InstanceObject = Instantiate(LootList[i],transform.position,Quaternion.Euler(0,0,0));
            InstanceObject.GetComponent<ObjectItem>().ActionQuick();
            yield return new WaitForSeconds(0.5f);
        }

        yield return new WaitForSeconds(0.5f);

        yield return new WaitForSeconds(0.1f);

        OnAction = false;
        StopCoroutine("TakeAction");

        yield return true;
    }
}
