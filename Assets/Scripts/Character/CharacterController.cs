﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController:MonoBehaviour {

    public static CharacterController Instance;

    public Vector3 CurrentPosition;

    [SerializeField] private Animator CharacterAnimator;
    [SerializeField] private Rigidbody CharacterRigidbody;

    [SerializeField] private float VerticalAxisValue;
    [SerializeField] private float HorizontalAxisValue;
    [SerializeField] private float SpeedMoving;

    private float angle;

    private void Awake() {
        Instance = this;
    }

    public void OnEnable() {
        Character.Dead += Dead;
    }

    public void OnDisable() {
        Character.Dead -= Dead;
    }

    private void FixedUpdate() {
        Move();
    }

    private void Move() {
        if(!State.Instance.isEqual(StateType.dead)) {
            VerticalAxisValue = Input.GetAxis("Vertical");
            HorizontalAxisValue = Input.GetAxis("Horizontal");

            if((Input.GetAxis("Vertical") != 0) || (Input.GetAxis("Horizontal") != 0))
                Rotation();

            if((Mathf.Abs(Input.GetAxis("Vertical")) > 0) || (Mathf.Abs(Input.GetAxis("Horizontal")) > 0))
                Walk();
            else
                CharacterAnimator.SetTrigger("Idle");
        }
    }

    private void Walk() {
        if((Mathf.Abs(Input.GetAxis("Vertical"))) > (Mathf.Abs(Input.GetAxis("Horizontal"))))
            CharacterAnimator.SetFloat("RunningSpeed",Mathf.Abs(Input.GetAxis("Vertical")));
        else
            CharacterAnimator.SetFloat("RunningSpeed",Mathf.Abs(Input.GetAxis("Horizontal")));

        CharacterAnimator.SetTrigger("Run");

        CurrentPosition = transform.position;
    }

    private void Rotation() {

        angle = Mathf.Atan2(VerticalAxisValue,-HorizontalAxisValue) * Mathf.Rad2Deg;
        CharacterRigidbody.MoveRotation(Quaternion.Euler(0,angle,0));

        CharacterRigidbody.MovePosition(transform.position + new Vector3(VerticalAxisValue,0,-HorizontalAxisValue) * Time.deltaTime * 7f);
    }

    public void Dead() {
        CharacterAnimator.SetTrigger("Dead");
        State.Instance.SetState(StateType.dead);
    }
}
