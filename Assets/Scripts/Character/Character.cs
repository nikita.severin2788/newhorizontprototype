﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
    public static Character Instance;

    public int Healtch;
    public int Oxygen;

    public int OxygenRemovePreSecond;
    public int HeltchRemovePreSecond;

    public static event UnityAction Dead;
    public static event UnityAction<int> HealtchUpdate;
    public static event UnityAction<int> OxygenUpdate;

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        HealtchUpdate?.Invoke(Healtch);
        OxygenUpdate?.Invoke(Oxygen);
        StartCoroutine("OxygenRemove");
        
    }


    //-----TakeDamage-----
    public void SetDamage(int damage) {
        if(Healtch + damage <= 0) {
            Healtch = 0;
            Dead?.Invoke();
           // State.Instance.SetState("Dead");
        } else {
            Healtch -= damage;
        }
        HealtchUpdate?.Invoke(Healtch);
    }
    //----Heltch-------
    public void AddHeltch(int heltchCount) {
        if(Healtch + heltchCount > 100) {
            Healtch = 100;
        } else {
            Healtch += heltchCount;
        }
        HealtchUpdate?.Invoke(Healtch);
    }
    //----Oxygen-------
    public void AddOxygen(int oxygenCount) {
        if(Oxygen + oxygenCount > 100) {
            Healtch = 100;
        } else {
            Oxygen += oxygenCount;
        }
        OxygenUpdate?.Invoke(Oxygen);
    }

    public void RemoveOxygen(int oxygenCount) {
        if(Oxygen + oxygenCount <= 0) {
            Healtch = 0;
            StartCoroutine("HeltchRemove");
        } else {
            StopCoroutine("HeltchRemove");
            Oxygen -= oxygenCount;
        }
        
        OxygenUpdate?.Invoke(Oxygen);
    }

    IEnumerator OxygenRemove() {
        while(true) {
            RemoveOxygen(OxygenRemovePreSecond);
            yield return new WaitForSeconds(1f);
        }
    }

    IEnumerator HeltchRemove() {
        while(true) {
            SetDamage(HeltchRemovePreSecond);
            yield return new WaitForSeconds(0.5f);
        }
    }

}
